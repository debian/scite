#!/bin/sh

#set -x
set -e

DOCDIR=debian/scite/usr/share/scite
#DOCDIR=scite/doc

for htmlfile in ${DOCDIR}/*.html
do
    for f in ${DOCDIR}/*.html ${DOCDIR}/*.png ${DOCDIR}/*.jpg
    do
        fname=$(basename $f)
        sed -i -r \
            -e "s|href=\"https?://www.scintilla.org/${fname}|href=\"${fname}|gI" \
            -e "s|url\(https?://www.scintilla.org/${fname}\)|url(${fname})|gI" \
            -e "s|src=\"https?://(www\.)?scintilla.org/${fname}|src=\"${fname}|gI" \
            ${htmlfile}
    done
done
